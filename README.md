# README #

---------------------------------------------------------------------------------------------
1- Introduction to Python with Frameworks Course :
--------------------------------------------------------------------------------
1- Install Ubuntu Virtual Machine
2- Install Anaconda framework and Jupiter 
3- Python Basic Syntax, Variable Types and Basic Operators
4- Decision Making and Loops
5- Python Strings, Lists, Tuples and Dictionary
6- Python Functions, Modules, Object Oriented and Exceptions Handling
7- NumPy Crash Course
8- Matplotlib Crash Course
9- Pandas Crash Course
10- Hand on Labs
---------------------------------------------------------------------------------
2- Python Data Treatment Crash course:
---------------------------------------------------------------------------------
1- How To Load Machine Learning Data 
2- Understand Your Data With Descriptive Statistics
3- Understand Your Data With Visualization
4- Prepare Your Data For Machine Learning
----------------------------------------------------------------------------------
3- Machine Learning Course : 
---------------------------------------------------------------------------------- 
1- Feature Selection For Machine Learning
2- Evaluate the Performance of Machine Learning Algorithms with Resampling
3- Machine Learning Algorithm Performance Metrics
4- Spot-Check Classiﬁcation Algorithms
5- Spot-Check Regression Algorithms
6- Compare Machine Learning Algorithms 
7- Automate Machine Learning Workﬂows with Pipelines
8- Save and Load Machine Learning Models
9- Classification Project
10- Regression Project
-----------------------------------------------------------------------------------
4- Deep learning Step by Step :
----------------------------------------------------------------------------------- 
1- Introduction to Theano
2- Introduction to TensorFlow
3- Introduction to Keras
4- Project: Develop Large Models on GPUs Cheaply In the Cloud
5- Crash Course In Multilayer Perceptrons
6- Develop Your First Neural Network With Keras
7- Evaluate The Performance of Deep Learning Models
8- Use Keras Models With Scikit-Learn For General Machine Learning
9- Project: Multiclass Classiﬁcation Of Flower Species
10- Project: Binary Classiﬁcation Of Sonar Returns
11- Project: Regression Of Boston House Prices
12- Save Your Models For Later With Serialization
13- Keep The Best Models During Training With Checkpointing
14- Understand Model Behavior During Training By Plotting History
15- Reduce Overﬁtting With Dropout Regularization
16- Crash Course In Convolutional Neural Networks
17- Project: Handwritten Digit Recognition
18- Project Object Recognition in Photographs

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact